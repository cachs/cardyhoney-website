import { Injectable } from '@angular/core';
import { SanitizedUpdate } from '../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

import { HoneyExtraction } from './impl/honey-extraction.update';
import { InternationalUpdate1 } from './impl/international-update-1.update';
import { BeeswaxHoneycomb } from './impl/beeswax-honeycomb.update';
import { LavenderOil } from './impl/lavender-oil.update';
import { BeeProjectGalaxy } from './impl/bee-project-gps.update';
import { ApiaryFebruary } from './impl/apiary-feb.update';
import { EcoWalk2021 } from './impl/eco-walk-2021.update';
import { BeeFriendlyReflections } from './impl/bee-friendly-reflections-sd';
import { InternationalUpdate2 } from './impl/international-update-2.update';
import { HoneySales } from './impl/honey-sales.update';

@Injectable({
  providedIn: 'root'
})
export class UpdatesService {

  constructor(private domSanitizer: DomSanitizer) { }

  getUpdates(): SanitizedUpdate[] {

    let updates: SanitizedUpdate[] = [];

    // Should *really* be done with a backend API and database but we haven't got much of an option.
    // Place new updates at the top of this list
    updates.push(new HoneySales(this.domSanitizer))
    updates.push(new BeeFriendlyReflections(this.domSanitizer));
    updates.push(new EcoWalk2021(this.domSanitizer));
    updates.push(new ApiaryFebruary(this.domSanitizer));
    updates.push(new BeeProjectGalaxy(this.domSanitizer));
    updates.push(new LavenderOil(this.domSanitizer));
    updates.push(new InternationalUpdate2(this.domSanitizer));
    updates.push(new BeeswaxHoneycomb(this.domSanitizer));
    updates.push(new InternationalUpdate1(this.domSanitizer));
    updates.push(new HoneyExtraction(this.domSanitizer));
    
    
    return updates;
  }

}
