import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class BeeFriendlyReflections implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "bee-friendly-reflections-sd";
    }

    getTitle(): string {
        return "Reflections on Sudesha School's Bee Friendly Project in Nepal";
    }

    getDate(): string {
        return "15th March 2021";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            Students from Sudesha School, in Lalitpur, Nepal have written reflections on their bee-friendly project and sharing session. Click on the images to view in full-size.
        </p>
    
        <div class="images">
            <a href="https://i.imgur.com/Yk1Pvac.jpg" target="_blank"><img src="https://i.imgur.com/Yk1Pvac.jpg" width=144></a>
            <a href="https://i.imgur.com/nS77qnx.jpg" target="_blank"><img src="https://i.imgur.com/nS77qnx.jpg" width=144></a>
            <a href="https://i.imgur.com/5GQvxfQ.jpg" target="_blank"><img src="https://i.imgur.com/5GQvxfQ.jpg" width=144></a>
            <a href="https://i.imgur.com/ZPSQhTw.jpg" target="_blank"><img src="https://i.imgur.com/ZPSQhTw.jpg" width=144></a>
            <a href="https://i.imgur.com/NQkgwf2.jpg" target="_blank"><img src="https://i.imgur.com/NQkgwf2.jpg" width=144></a>
        </div>
        `);
    }

}