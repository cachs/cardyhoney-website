import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class LavenderOil implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "lavender-oil";
    }

    getTitle(): string {
        return "Extracting our bee-friendly lavender oil through distillation";
    }

    getDate(): string {
        return "1st Feburary 2021";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            Long time, no see! The second wave of COVID-19 has hindered our progress, however we're still
            doing everything we can to protect bees and help our cause. At the start of 2021, Kieran and Emily,
            two of our ambassadors, have been extracting lavender oil from our school-grown bee-friendly lavender. 
            Watch the video below to find out how it is done!
        </p>
    
        <iframe class="hor" src="https://player.vimeo.com/video/508798723" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        `);
    }

}