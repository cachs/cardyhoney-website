import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class InternationalUpdate1 implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "international-update-1";
    }

    getTitle(): string {
        return "An update from our international partner schools";
    }

    getDate(): string {
        return "19th October 2020";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <style>
            .galaxy-images img {
                width: 252px;
                height: 189px;
            }
        </style>

        <p>
            Due to the Covid-19 pandemic, <a class="nice_link" href="https://sudesha.edu.np/">The Sudesha School</a> are currently running distance-mode classes and cannot 
            involve their students to create bee-friendly environments on the school premises. However, the pupils are
            still doing their best to support our cause. They are currently learning about bees, bee-friendly environments,
            the importance of bees to humanity, and the honey of Nepal.
        </p>

        <p>
            Similarly, <a class="nice_link" href="https://www.facebook.com/OfficialPageGalaxy">Galaxy Public School</a> in Nepal cannot do many in-person activities such as beekeeping. However,
            some staff and students who live near the school have been taking care of the hives in-person to ensure continued
            support to the bees. The two hives pictured below were installed in February 2020, and the bee population since then
            has multiplied! 
        </p>

        <div class="galaxy-images">
            <img src="https://i.imgur.com/cvMCyAg.jpg">
            <img src="https://i.imgur.com/ijZc0nM.jpg">
            <img src="https://i.imgur.com/FMykqAJ.jpg">
        </div>

        <p>
        We are currently planning a video chat with our international schools to plan our next steps and discuss our progress
        so far.
    </p>
        `);
    }

}