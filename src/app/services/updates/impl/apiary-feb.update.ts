import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class ApiaryFebruary implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "apiary-feb";
    }

    getTitle(): string {
        return "Our apiary in February, by Holly.";
    }

    getDate(): string {
        return "4th March 2021";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            In this video, Holly explains our apiary setup and how we use it for the production of honey.
        </p>
    
        <iframe width="560" height="315" class="hor" src="https://www.youtube.com/embed/VGO3o6HmS7c" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        `);
    }

}