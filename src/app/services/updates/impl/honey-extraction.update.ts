import { SafeHtml } from '@angular/platform-browser';
import { SanitizedUpdate } from '../../../models/update.model';
import { DomSanitizer } from '@angular/platform-browser';

export class HoneyExtraction implements SanitizedUpdate {

    constructor(private domSanitizer: DomSanitizer) {
    }

    getId(): string {
        return "honey-extraction";
    }

    getTitle(): string {
        return "How do we extract honey? Our new video, starring Holly";
    }

    getDate(): string {
        return "10th October 2020";
    }

    getHtmlContent(): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustHtml(`
        <p>
            Honey extraction is a complicated process. We wanted to shed some light on how it's done.
            Watch our new video below to get to know us and how we work!
        </p>
    
        <iframe class="hor" src="https://player.vimeo.com/video/466888410" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        `);
    }

}