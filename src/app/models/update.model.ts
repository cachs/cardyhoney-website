import { SafeHtml } from '@angular/platform-browser';

export interface SanitizedUpdate {

    getId(): string;
    getTitle(): string;
    getDate(): string;

    getHtmlContent(): SafeHtml;
}