import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './common/navigation/navigation.component';
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { FooterComponent } from './common/footer/footer.component';
import { CommiteeComponent } from './pages/commitee/commitee.component';

import { GalleryComponent } from './pages/gallery/gallery.component';
import { SalesComponent } from './pages/sales/sales.component';
import { UpdatesComponent } from './pages/updates/updates.component';
// import { BeehivePageComponent } from './pages/beehive-page/beehive-page.component';
import { HollyGardenComponent } from './pages/holly-garden/holly-garden.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AboutComponent,
    SalesComponent,
    HomeComponent,
    FooterComponent,
    CommiteeComponent,
    GalleryComponent,
    UpdatesComponent,
    // BeehivePageComponent,
    HollyGardenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
