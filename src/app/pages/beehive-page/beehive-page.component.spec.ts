import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BeehivePageComponent } from './beehive-page.component';

describe('BeehivePageComponent', () => {
  let component: BeehivePageComponent;
  let fixture: ComponentFixture<BeehivePageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BeehivePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeehivePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
