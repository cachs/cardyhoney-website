import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CommiteeComponent } from './commitee.component';

describe('CommiteeComponent', () => {
  let component: CommiteeComponent;
  let fixture: ComponentFixture<CommiteeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CommiteeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommiteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
