# CardyHoney

Front-end for [cardyhoney.com](http://cardyhoney.com), currently used as an information portal for the CardyHoney project by [Cardinal Allen High School](http://cardinalallen.co.uk).

<!-- Created utilising Angular 10 and Sass CSS. Commit messages should follow the [Conventional Commit standards](https://www.conventionalcommits.org/). -->

**Contributors -**
 - [Mark Harding](https://gitlab.com/markeharding)
 - [Jacob Passam](https://gitlab.com/jacob_passam)
 - [Lewis Garside](https://gitlab.com/L3W15_GAR51) 

 

 
